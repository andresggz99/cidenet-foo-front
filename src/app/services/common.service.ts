import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IdentificationType} from "../models/identification-type";
import {Country} from "../models/country";
import {Division} from "../models/division";

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  baseUrlV1 = environment.baseUrlV1;

  constructor(private http: HttpClient) {
  }

  findIdentificationTypes(): Observable<IdentificationType[]>{
    return this.http.get<IdentificationType[]>(`${this.baseUrlV1}/identification-types`);
  }

  findCountries(): Observable<Country[]>{
    return this.http.get<Country[]>(`${this.baseUrlV1}/countries`);
  }

  findDivisions(): Observable<Division[]>{
    return this.http.get<Country[]>(`${this.baseUrlV1}/divisions`);
  }
}
