import { Injectable } from '@angular/core';
import {UserResponsePagination, UserSaveRequest, UserSaveResponse, UserSearchQuery} from "../models/user";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseUrlV1 = environment.baseUrlV1;

  constructor(private http: HttpClient) {
  }

  create(userToCreate: UserSaveRequest) {
    return this.http.post(`${this.baseUrlV1}/users`, userToCreate);
  }

  findByParameters(page: number, size: number, userSearchQuery: UserSearchQuery): Observable<UserResponsePagination> {

    return this.http.get<UserResponsePagination>(`${this.baseUrlV1}/users`, {
      params:{
        page: page,
        size: size,
        ...(this.clean(userSearchQuery))
      }
    });
  }

  clean(obj) {
    for (var propName in obj) {
      if (obj[propName] === null || obj[propName] === undefined || obj[propName] === '') {
        delete obj[propName];
      }
    }
    return obj
  }

  deleteById(id: number) {
    return this.http.delete(`${this.baseUrlV1}/users/${id}`);
  }
}
