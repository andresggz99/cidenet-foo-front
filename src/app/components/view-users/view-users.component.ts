import {Component, Input, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {UserSearchQuery} from "../../models/user";
import {UserService} from "../../services/user.service";
import Swal from "sweetalert2";

@Component({
  selector: 'app-view-users',
  templateUrl: './view-users.component.html',
  styleUrls: ['./view-users.component.css']
})
export class ViewUsersComponent implements OnInit {

  @Input() userSearchQuery: UserSearchQuery;

  usersFound: any[];
  dataSource = new MatTableDataSource<any>();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['id', 'identificationType', 'identification', 'firstName', 'middleName',
    'primaryLastName', 'secondLastName', 'email', 'division', 'laborCountry', 'enrollmentDate',
    'createDate', 'updateDate', 'actions'];


  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    this.getData(0, 5);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.getData(0, 5);
  }


  getData(page, size){
    this.userService.findByParameters(page, size, this.userSearchQuery).subscribe(res => {

      this.usersFound = res.result;
      this.usersFound.length = res.total;

      this.dataSource = new MatTableDataSource<any>(this.usersFound);
      this.dataSource.paginator = this.paginator;
    })
  }

  getNextData(userSearchQuery: UserSearchQuery, currentSize, page, size) {

    this.userService.findByParameters(page, size, userSearchQuery).subscribe(res => {
      this.usersFound.length = currentSize;
      this.usersFound.push(... res.result);

      this.usersFound.length = res.total;

      this.dataSource = new MatTableDataSource<any>(this.usersFound);
      this.dataSource._updateChangeSubscription();

      this.dataSource.paginator = this.paginator;
    })
  }

  pageChanged(event){
    let pageIndex = event.pageIndex;
    let pageSize = event.pageSize;

    let previousIndex = event.previousPageIndex;

    let previousSize = pageSize * pageIndex;

    this.getNextData(this.userSearchQuery, previousSize, pageIndex, pageSize);
  }

  deleteById(id) {

    let userToDelete = this.usersFound.find(user => user.id = id);

    Swal.fire({
      title: `Are you sure? User ${userToDelete.firstName}
      ${userToDelete.primaryLastName} (${userToDelete.identificationType.name} ${userToDelete.identification}) `,
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.userService.deleteById(id).subscribe(res => {
            Swal.fire(
              'Deleted!',
              'User has been deleted.',
              'success'
            )
          this.getData(0, 10);

          }, error => {
            Swal.fire({
              icon: 'error',
              title: 'Error',
              confirmButtonColor: '#542E87',
              text: 'An error occurred deleting the user'
            })
          }
        )
      }
    })
  }


}
