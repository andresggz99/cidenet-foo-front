import {Component, OnInit} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {CommonService} from "../../services/common.service";
import {Country} from "../../models/country";
import {Division} from "../../models/division";
import {IdentificationType} from "../../models/identification-type";
import {UserManagementComponent} from "../user-management/user-management.component";
import {UserSearchQuery} from "../../models/user";

@Component({
  selector: 'app-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.css']
})
export class UserHomeComponent implements OnInit {

  countries: Country[];
  divisions: Division[];
  identificationTypes: IdentificationType[];

  searchUserByParams: UserSearchQuery;

  constructor(private dialog: MatDialog,
              private commonService: CommonService) {
    this.countries = [];
    this.divisions = [];
    this.identificationTypes = [];
  }

  ngOnInit(): void {
    this.commonService.findCountries().subscribe(countries =>
      this.countries = countries
    );

    this.commonService.findIdentificationTypes().subscribe(identificationTypes =>
      this.identificationTypes = identificationTypes
    )

    this.commonService.findDivisions().subscribe(divisions => this.divisions = divisions);
  }

  createNewUser(): void {
    const dialogRef = this.dialog.open(UserManagementComponent, {

      data: {
        identificationTypes: this.identificationTypes,
        countries: this.countries,
        divisions: this.divisions,
        register: true
      }
    });

  }

  findByParameters() {
    const dialogRef = this.dialog.open(UserManagementComponent, {

      data: {
        identificationTypes: this.identificationTypes,
        countries: this.countries,
        divisions: this.divisions,
        register: false
      }
    });

    dialogRef.afterClosed().subscribe(res => {
      if(res.data){
        this.searchUserByParams = res.data;
      }
    })
  }
}
