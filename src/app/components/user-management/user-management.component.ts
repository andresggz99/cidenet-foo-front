import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {UserSaveRequest, UserSearchQuery} from "../../models/user";
import {DatePipe} from "@angular/common";
import {UserService} from "../../services/user.service";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register-user',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {

  userFormGroup: FormGroup;
  userToCreate: UserSaveRequest;
  userSearchQuery: UserSearchQuery;
  enrollmentMinDate: Date;
  enrollmentMaxDate: Date;
  register: true;

  constructor(public dialogRef: MatDialogRef<UserManagementComponent>,
              private formBuilder: FormBuilder,
              private userService: UserService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.register = data.register;
    this.enrollmentMaxDate = new Date();
    this.enrollmentMinDate = new Date(new Date().setMonth(new Date().getMonth() -1));
    this.buildForm();
  }

  ngOnInit(): void {
  }

  private buildForm(): void{

    if(this.register){
      this.userFormGroup = this.formBuilder.group(
        {
          identificationType: ['', [Validators.required]],
          identification: ['', [Validators.required, Validators.maxLength(20), Validators.pattern("[A-Z0-9a-z\-]+")]],
          firstName: ['', [Validators.required, Validators.maxLength(20), Validators.pattern("[A-Z]+")]],
          middleName: [null, [Validators.maxLength(50), Validators.pattern("[A-Z\\s]*")]],
          primaryLastName: ['', [Validators.required, Validators.maxLength(20), Validators.pattern("[A-Z\\s]+")]],
          secondLastName: ['', [Validators.required, Validators.maxLength(20), Validators.pattern("[A-Z\\s]+")]],
          division: ['', [Validators.required]],
          laborCountry: ['', [Validators.required]],
          enrollmentDate: ['', [Validators.required]],

        }
      )
    }else{
      this.userFormGroup = this.formBuilder.group(
        {
          id: [null, [Validators.pattern("[0-9]+")]],
          identificationType: [null, []],
          identification: [null, [ Validators.maxLength(20)]],
          firstName: [null, [ Validators.maxLength(20)]],
          middleName: [null, [Validators.maxLength(50)]],
          primaryLastName: [null, [Validators.maxLength(20)]],
          secondLastName: [null, [Validators.maxLength(20)]],
          division: [null, []],
          laborCountry: [null, []],
          enrollmentDate: [null, []],
          email: [null, [Validators.email]],
          createDate: [null, []],
          updateDate: [null, []],
        }
      )
    }

  }

  onNoClick(): void{
    this.dialogRef.close();
  }

  findByParamaters(){
    this.setupUserSearchQuery();
    this.dialogRef.close({data: this.userSearchQuery})
  }

  createUser(){
    this.setupUserToCreate();
    this.userService.create(this.userToCreate).subscribe(r => {
      this.dialogRef.close();
      Swal.fire('The user was created successfully')
    }, res => {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        confirmButtonColor: '#542E87',
        text: res.error.details || 'An error occurred creating the user'
      })
    })
  }

  setupUserSearchQuery(){

    this.userSearchQuery = {
      id: this.userFormGroup.get('id')?.value,
      identification: this.userFormGroup.get('identification')?.value,
      identificationTypeId: this.userFormGroup.get('identificationType')?.value,
      firstName: this.userFormGroup.get('firstName')?.value,
      middleName: this.userFormGroup.get('middleName')?.value,
      primaryLastName: this.userFormGroup.get('primaryLastName')?.value,
      secondLastName: this.userFormGroup.get('secondLastName')?.value,
      divisionId: this.userFormGroup.get('division')?.value,
      laborCountryId: this.userFormGroup.get('laborCountry')?.value,
      email: this.userFormGroup.get('email')?.value,
      enrollmentDate: (new DatePipe('en-US').transform((this.userFormGroup.get('enrollmentDate')?.value), 'yyyy-MM-dd')),
      createDate: (new DatePipe('en-US').transform((this.userFormGroup.get('createDate')?.value), 'yyyy-MM-dd')),
      updateDate: (new DatePipe('en-US').transform((this.userFormGroup.get('updateDate')?.value), 'yyyy-MM-dd'))
    }
  }

  setupUserToCreate(){

    this.userToCreate = {
      identification: this.userFormGroup.get('identification')?.value,
      identificationTypeId: this.userFormGroup.get('identificationType')?.value,
      firstName: this.userFormGroup.get('firstName')?.value,
      middleName: this.userFormGroup.get('middleName')?.value,
      primaryLastName: this.userFormGroup.get('primaryLastName')?.value,
      secondLastName: this.userFormGroup.get('secondLastName')?.value,
      divisionId: this.userFormGroup.get('division')?.value,
      laborCountryId: this.userFormGroup.get('laborCountry')?.value,
      enrollmentDate: (new DatePipe('en-US').
      transform((this.userFormGroup.get('enrollmentDate')?.value), 'yyyy-MM-dd'))
    }
  }

}
