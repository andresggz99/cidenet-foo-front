import {IdentificationType} from "./identification-type";
import {Division} from "./division";
import {Country} from "./country";

export interface UserSaveResponse{
  id: number;
  identificationType: IdentificationType;
  identification: string;
  firstName: string;
  middleName?: string;
  primaryLastName: string;
  secondLastName: string;
  division: Division;
  laborCountry: Country;
  email: string;
  enrollmentDate: string;
  createDate: Date;
  updateDate: Date;
}

export interface UserSaveRequest{
  identificationTypeId: number;
  identification: string;
  firstName: string;
  middleName?: string;
  primaryLastName: string;
  secondLastName: string;
  divisionId: number;
  laborCountryId: number;
  enrollmentDate: string;
}

export interface UserSearchQuery{
  id?: number;
  identificationTypeId?: number;
  identification?: string;
  firstName?: string;
  middleName?: string;
  primaryLastName?: string;
  secondLastName?: string;
  divisionId?: number;
  laborCountryId?: number;
  email?: string;
  enrollmentDate?: string;
  createDate?: string;
  updateDate?: string;
}

export interface UserResponsePagination {

  result: UserSaveResponse[],
  total: number,
  page: number,
  returnedRecords: number
}
